package handler

import (
	"backend-go/book"
	"fmt"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

// Membuat module nanti diakses lewat import dan nama func. harus diawal huruf besar karena
// supaya bisa public
var dsn = "root:root@tcp(localhost:3306)/pustaka-api?charset=utf8mb4&parseTime=True&loc=Local"
var db, errDb = gorm.Open(mysql.Open(dsn), &gorm.Config{})

func CheckingConnectToDB() {
	if errDb != nil {
		log.Fatal("DB Connection error")
	}
	fmt.Println("DB connected succeed")
}

func RootHandler(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"name":  "Azis Zuhri Pratomo",
		"kelas": "1 D4 IT A",
	})
}

func HelloFunc(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"title":   "Selamat Datang!",
		"message": "Hello World",
	})
}

func BooksHandler(c *gin.Context) {
	id := c.Param("id")
	c.JSON(http.StatusOK, gin.H{
		"id": id,
	})
}

func QueryBooks(c *gin.Context) {
	id := c.Query("id")
	title := c.Query("title")
	c.JSON(http.StatusOK, gin.H{
		"id":    id,
		"title": title,
	})
}

func PostBooksHandler(c *gin.Context) {
	var data book.Books

	err := c.ShouldBindJSON(&data)

	if err != nil {
		dataErrorMsg := []string{}
		for _, e := range err.(validator.ValidationErrors) {
			errorMessage := fmt.Sprintf("Error di field %s, kondisi %s", e.Field(), e.ActualTag())
			dataErrorMsg = append(dataErrorMsg, errorMessage)
		}
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  false,
			"message": dataErrorMsg,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"title": data.Title,
		"price": data.Price,
	})
}

func CreateBook(c *gin.Context) {
	CheckingConnectToDB()

	var dataBody book.Books
	err := c.ShouldBindJSON(&dataBody)
	if err != nil {
		dataErrorMsg := []string{}
		for _, e := range err.(validator.ValidationErrors) {
			errorMessage := fmt.Sprintf("Error di field %s, kondisi %s", e.Field(), e.ActualTag())
			dataErrorMsg = append(dataErrorMsg, errorMessage)
		}
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  false,
			"message": dataErrorMsg,
		})
		return
	}

	var checkingTitleBook book.Books
	errCheckingTitleBook := db.Where("title LIKE ? ", dataBody.Title).First(&checkingTitleBook).Error
	if errCheckingTitleBook == nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  false,
			"message": "Maaf.. Buku dengan judul " + checkingTitleBook.Title + " telah dibuat sebelumnya",
		})
		return
	}

	newBook := book.Books{}
	newBook.Title = dataBody.Title
	newBook.Description = dataBody.Description
	newBook.Price = dataBody.Price
	newBook.Rating = dataBody.Rating
	err = db.Create(&newBook).Error
	if err != nil {
		fmt.Println("ERROR TO CREATING BOOK")
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  false,
			"message": err,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"status":  true,
		"message": "Berhasil membuat buku",
		"data":    newBook,
	})
}

func GetBook(c *gin.Context) {
	CheckingConnectToDB()
	getIdBook := c.Param("id")

	var findBook book.Books
	err := db.First(&findBook, getIdBook).Error
	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"status":  false,
			"message": "Data tidak ditemukan",
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"status":  true,
		"message": "Sukses mengambil data",
		"data":    findBook,
	})
}

func UpdateBook(c *gin.Context) {
	CheckingConnectToDB()
	var dataBody book.BookUpdate
	err := c.ShouldBindJSON(&dataBody)
	if err != nil {
		dataErrorMsg := []string{}
		for _, e := range err.(validator.ValidationErrors) {
			errorMessage := fmt.Sprintf("Error di field %s, kondisi %s", e.Field(), e.ActualTag())
			dataErrorMsg = append(dataErrorMsg, errorMessage)
		}
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  false,
			"message": dataErrorMsg,
		})
		return
	}

	var updateBook book.Books
	err = db.First(&updateBook, dataBody.ID).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  false,
			"message": "Maaf... data buku tidak ada",
		})
		return
	}

	updateBook.Title = dataBody.Title
	updateBook.Description = dataBody.Description
	updateBook.Price = dataBody.Price
	updateBook.Rating = dataBody.Rating
	err = db.Save(&updateBook).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  false,
			"message": err.Error(),
		})

		return
	}

	c.JSON(http.StatusBadRequest, gin.H{
		"status":  true,
		"message": "Berhasil mengedit data buku",
		"data":    updateBook,
	})
}
