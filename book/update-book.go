package book

type BookUpdate struct {
	ID          int    `json:"id" binding:"required"`
	Title       string `json:"title"`
	Price       int    `json:"price" binding:"number"`
	Description string `json:"description"`
	Rating      int    `json:"rating" binding:"number"`
}
