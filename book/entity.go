package book

import "time"

type BookTable struct {
	ID          int    `gorm:"primaryKey;type:int"`
	Title       string `gorm:"type:varchar(50)"`
	Description string
	Price       int `gorm:"type:int"`
	Rating      int `gorm:"type:int"`
	CreatedAt   time.Time
	UpdatedAt   time.Time
}
