package main

import (
	"backend-go/handler"

	"github.com/gin-gonic/gin"
)

func main() {

	r := gin.Default()

	v1 := r.Group("/v1") // grouping routing

	v1.GET("/", handler.RootHandler)
	v1.GET("/hello", handler.HelloFunc)

	// Books Handler
	v1.GET("/books/:id", handler.BooksHandler) // Endpoint

	// Books Query Params
	v1.GET("/books/query", handler.QueryBooks)

	// Post Book Handler
	v1.POST("/books", handler.PostBooksHandler)

	// CRUD API
	v1.POST("/book", handler.CreateBook)
	v1.GET("/book/:id", handler.GetBook)
	v1.PUT("/book/", handler.UpdateBook)

	r.Run() //r.Run(":5000") -> custom ports
}
